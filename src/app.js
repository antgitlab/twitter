require("dotenv").load();
const Twitter = require("twit");

// the following can be set as variables when running as a container, each of these defaults to a value for easy running in dev outside of container
let twitterTrack = process.env.twitterTrack
twitterTrack = twitterTrack.toLowerCase();
twitterTrack = twitterTrack.replace(' ', '_')

const outputKafka = (process.env.outputKafka || 'false') === 'true' ? true : false
const outputElastic = (process.env.outputElastic || 'true') === 'true' ? true : false
const elasticProtocol = process.env.protocol || 'https'
const elasticHost = process.env.elasticHost
const elasticPort = process.env.elasticPort
const elasticUsername = process.env.elasticUsername
const elasticPassword = process.env.elasticPassword
const elasticTopic = process.env.elasticTopic || twitterTrack
const elasticApiVersion = process.env.elasticApiVersion
const kafkaProtocol = process.env.protocol || 'http'
const kafkaHost = process.env.kafkaHost
const kafkaPort = process.env.kafkaPort
const kafkaTopic = process.env.kafkaTopic || twitterTrack
const TWITTER_CONSUMER_KEY = process.env.TWITTER_CONSUMER_KEY
const TWITTER_CONSUMER_SECRET = process.env.TWITTER_CONSUMER_SECRET
const TWITTER_ACCESS_TOKEN_KEY = process.env.TWITTER_ACCESS_TOKEN_KEY
const TWITTER_ACCESS_TOKEN_SECRET = process.env.TWITTER_ACCESS_TOKEN_SECRET
const twitterJsonOrMessage = "message";

const elasticsearch = require('@elastic/elasticsearch')
const elasticClient = new elasticsearch.Client({
  node: `${elasticProtocol}://${elasticHost}:${elasticPort}`,
  log: 'info',
  apiVersion: elasticApiVersion,
  // auth: {
  //   apiKey: {
  //     id: '06QV5HsBP8Ajk0QCuRCf',
  //     api_key: 'fQH7x-CGQLOlNRQVYUP4GA'
  //   }
  // },
  auth: {
    username: `${elasticUsername}`,
    password: `${elasticPassword}`
  },
  ssl: {
    // ca: fs.readFileSync('./cacert.pem'),
    rejectUnauthorized: false
  }
});

if (!twitterTrack) throw `Must pass an argument as in 'node app javascript'`;

if (twitterJsonOrMessage !== "json" && twitterJsonOrMessage !== "message")
  throw `twitterJsonOrMessage must be 'json' or 'message'`;

var twitterClient = new Twitter({
  consumer_key:         TWITTER_CONSUMER_KEY,
  consumer_secret:      TWITTER_CONSUMER_SECRET,
  access_token:         TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret:  TWITTER_ACCESS_TOKEN_SECRET,
  timeout_ms:           60*1000 // optional HTTP request timeout to apply to all requests.
  // strictSSL:            true,     // optional - requires SSL certificates to be valid.
})

var twitterStream = twitterClient.stream("statuses/filter", {
  track: `${twitterTrack}`,
  has: 'profile_geo',
  geocode: true
});

const kafkaOptions = {
  kafkaHost: kafkaHost + ':' + kafkaPort,
  topic: kafkaTopic,
  sessionTimeout: 15000,
  protocol: ['roundrobin'],
  asyncPush: false,
};

if (outputKafka) {
  var kafka = require('kafka-node'),
    HighLevelProducer = kafka.HighLevelProducer,
    twitterClient = new kafka.KafkaClient(kafkaOptions),
    kafkaProducer = new HighLevelProducer(twitterClient)
}

twitterStream.on("tweet", async function(event) {
  try {
    if (twitterJsonOrMessage === "message") {
      let ok = true
      try {
        event.place.bounding_box.coordinates !== undefined
      }
      catch (err) {
        ok = false
      }
      if (!ok) return

      delete event.user
      delete event.extended_tweet
      delete event.retweeted_status
      delete event.entities
      delete event.extended_entities
      delete event.quoted_status
      event.location = {}
      event.location.lon = event.place.bounding_box.coordinates[0][0][0]
      event.location.lat = event.place.bounding_box.coordinates[0][0][1]
      event["@timestamp"] = new Date(parseInt(event.timestamp_ms))

      if (outputKafka) {
        console.log('outputKafka: ' + event.text);
        if (kafkaProducer.ready) {
          payloads = [
            { topic: kafkaTopic, messages:  JSON.stringify(event)}
          ]
          kafkaProducer.send(payloads, function (err, tweet) {
            if (tweet) console.log(tweet) ;
            if (err) console.log(err) ;
          });
          kafkaProducer.on('error', function (err) {
              console.log(`Error ${err}`)
          })
        }
      }
      if (outputElastic) {
        console.log('outputElastic: ' + event.text)
        try {
          await elasticClient.create({
            id: event.id,
            type: "_doc",
            index: `twitter_elastic_${elasticTopic}`,
            body: event
          })
        } catch (err) {
          console.log('*** axios err')
          console.log(err)
        }
      }
    } else {
      console.log('*** else err')
    }
  } catch (err) {
    console.log('*** twitterStream.on date err')
    console.log(err)
  }
})

twitterStream.on("error", function(error) {
  console.log('***twitterStream error')
  console.log(error)
  throw error;
});
